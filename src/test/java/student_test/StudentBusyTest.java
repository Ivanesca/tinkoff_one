package student_test;

import org.junit.jupiter.api.Test;
import ru.turnip.model.Student;
import ru.turnip.utils.student.BusyStudents;
import ru.turnip.utils.student.StudentParser;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StudentBusyTest {

    Clock clock = Clock.fixed(Instant.parse("2012-12-12T15:12:12.00Z"), ZoneId.of("Europe/London"));

    @Test
    public void getBusyStudentTest() {
        BusyStudents busyStudents = new BusyStudents(clock, new StudentParser());
        List<Student> studentList = busyStudents.getBusyStudents();
        int mockHour = LocalDateTime.now(clock).getHour();
        for (Student student : studentList) {
            assertTrue(student.getTimeFrom() < mockHour);
            assertTrue(student.getTimeTo() >= mockHour);
        }
    }
}
