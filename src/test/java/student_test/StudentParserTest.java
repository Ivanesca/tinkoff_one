package student_test;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import ru.turnip.model.Student;
import ru.turnip.utils.student.StudentParser;


public class StudentParserTest {
    List<Student> studentList = List.of(
            new Student("Беляев Никита Сергеевич", 19, 8, 14),
            new Student("Борисов Илья Александрович", 21, 8, 14)
    );

    StudentParser studentParser = new StudentParser();

    @Test
    public void parseTest() {
        List<Student> parsedStudents = studentParser.parse("src/test/resources/test_students.csv", 1, ",");

        assertTrue(conditionallyEquals(parsedStudents.get(0), studentList.get(0)));
        assertTrue(conditionallyEquals(parsedStudents.get(1), studentList.get(1)));
    }

    private boolean conditionallyEquals(Student student1, Student student2) {
        return student1.getName().equals(student2.getName()) &&
                student1.getAge() == student2.getAge() &&
                student1.getTimeFrom() == student2.getTimeFrom() &&
                student1.getTimeTo() == student2.getTimeTo();
    }
}
