package student_test;

import ru.turnip.model.Student;
import ru.turnip.utils.student.StudentUtils;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class StudentUtilsTest {
    List<Student> studentList = List.of(
            new Student("Vasco da Gama", 23),
            new Student("Francis Drake", 21),
            new Student("Fernando Magallanes", 23),
            new Student("Bartomeu Dias", 24),
            new Student("Miguel Corte-Real", 24));

    @Test
    public void testTotalAge() {
        assertEquals(115, StudentUtils.totalAge(studentList));
    }

    @Test
    public void testGetNames() {
        Set<String> names = Set.of(
                "Vasco da Gama",
                "Francis Drake",
                "Fernando Magallanes",
                "Bartomeu Dias",
                "Miguel Corte-Real");
        assertEquals(names, StudentUtils.getNames(studentList));
    }

    @Test
    public void testIsThereOlder() {
        assertTrue(StudentUtils.isThereOlder(studentList, 23));
        assertFalse(StudentUtils.isThereOlder(studentList, 25));
    }

    @Test
    public void testToNamesMap() {
        Map<UUID, String> namesMap = new HashMap<>();
        for (Student student : studentList) {
            namesMap.put(student.getId(), student.getName());
        }
        assertEquals(namesMap, StudentUtils.toNamesMap(studentList));
    }

    @Test
    public void testToCollectionsMap() {
        Map<Integer, List<Student>> collectionsMap = new HashMap<>();
        for (Student student : studentList) {
            collectionsMap.merge(
                    student.getAge(),
                    new ArrayList<>(Collections.singletonList(student)),
                    (prev, cur) -> {
                        prev.addAll(cur);
                        return prev;
                    });
        }
        assertEquals(collectionsMap, StudentUtils.toCollectionsMap(studentList));
    }
}
