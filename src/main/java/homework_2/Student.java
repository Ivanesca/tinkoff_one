package homework_2;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
public class Student {
    private UUID id;
    private String name;
    private int age;

    public Student(String name, int age) {
        id = UUID.randomUUID();
        this.name = name;
        this.age = age;
    }
}
