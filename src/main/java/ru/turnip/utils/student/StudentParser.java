package ru.turnip.utils.student;

import org.springframework.stereotype.Component;
import ru.turnip.model.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class StudentParser {
    public List<Student> parse(String file, int startRow, String delimiter)  {
        List<Student> students = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(file))) {
            for (int i = 0; i < startRow; i++) {
                br.readLine();
            }
            String line;
            while ((line = br.readLine()) != null) {
                String[] columns = line.split(delimiter);
                    students.add(new Student(
                            columns[0].replace("\"", ""),
                            Integer.parseInt(columns[1]),
                            Integer.parseInt(columns[2]),
                            Integer.parseInt(columns[3])
                    ));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return students;
    }
}
