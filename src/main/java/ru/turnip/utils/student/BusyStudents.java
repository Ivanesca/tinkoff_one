package ru.turnip.utils.student;

import org.springframework.stereotype.Component;
import ru.turnip.aspect.CountStudents;
import ru.turnip.model.Student;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BusyStudents {
    private final Clock clock;
    private final StudentParser studentParser;

    public static final Map<String, Integer> studentMap = new HashMap<>();

    public BusyStudents(Clock clock, StudentParser studentParser) {
        this.clock = clock;
        this.studentParser = studentParser;
    }

    @CountStudents
    public List<Student> getBusyStudents() {
        List<Student> allStudents = studentParser.parse("src/main/resources/students.csv", 1, ",");
        List<Student> matchedStudents = new ArrayList<>();
        int nowHour = LocalDateTime.now(clock).getHour();
        for (Student student : allStudents) {
            if (nowHour >= student.getTimeFrom() && nowHour < student.getTimeTo()) {
                matchedStudents.add(student);
            }
        }
        return matchedStudents;
    }
}
