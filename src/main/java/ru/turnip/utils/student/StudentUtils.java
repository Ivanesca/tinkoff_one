package ru.turnip.utils.student;

import ru.turnip.model.Student;
import java.util.*;
import java.util.stream.Collectors;

public class StudentUtils {

    public static int totalAge(List<Student> students) {
        return students.stream()
                .map(Student::getAge)
                .reduce(Integer::sum)
                .orElse(-1);
    }

    public static Set<String> getNames(List<Student> students) {
        return students.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());
    }

    public static boolean isThereOlder(List<Student> students, int age) {
        return students.stream()
                .anyMatch(student -> student.getAge() > age);
    }

    public static Map<UUID, String> toNamesMap(List<Student> students) {
        return students.stream()
                .collect(Collectors.toMap(
                        Student::getId,
                        Student::getName
                ));
    }

    public static Map<Integer, List<Student>> toCollectionsMap(List<Student> students) {
        return students.stream()
                .collect(Collectors.groupingBy(
                        Student::getAge,
                        Collectors.toList()
                ));
    }
}
