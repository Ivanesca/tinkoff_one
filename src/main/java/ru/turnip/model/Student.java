package ru.turnip.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
public class Student {
    private UUID id;
    private String name;
    private int age;
    private int timeFrom;
    private int timeTo;

    public Student(String name, int age) {
        this(name, age, 10, 12);
    }

    public Student(String name, int age, int time_from, int time_to) {
        id = UUID.randomUUID();
        this.name = name;
        this.age = age;
        this.timeFrom = time_from;
        this.timeTo = time_to;
    }
}
