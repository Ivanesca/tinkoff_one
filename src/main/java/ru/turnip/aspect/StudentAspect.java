package ru.turnip.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import ru.turnip.model.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Aspect
public class StudentAspect {
    public static final Map<String, Integer> busyStudentMap = new HashMap<>();

    @AfterReturning(pointcut = "@annotation(CountStudents)", returning = "busyStudents")
    public void afterGetBusyStudentsAdvice(List<Student> busyStudents) {
        for (Student student : busyStudents) {
            busyStudentMap.merge(student.getName(), 1, Integer::sum);
        }
    }

}
