package ru.turnip.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.turnip.model.Student;
import ru.turnip.utils.student.BusyStudents;

import java.util.logging.Logger;

@Service
public class StudentService {
    private final BusyStudents busyStudents;

    public StudentService(BusyStudents busyStudents) {
        this.busyStudents = busyStudents;
    }

    @Scheduled(fixedDelay = 3600000)
    public void checkStudents() {
        busyStudents.getBusyStudents();
    }

}
